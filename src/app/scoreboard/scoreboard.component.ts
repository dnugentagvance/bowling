import { Component, OnInit } from '@angular/core';
import { GameService } from '../services/game.service';
import { Game } from '../models/game';

@Component({
  selector: 'app-scoreboard',
  templateUrl: './scoreboard.component.html',
  styleUrls: ['./scoreboard.component.css']
})
export class ScoreboardComponent implements OnInit {
  public currentGame: Game;

  constructor(private readonly gameService: GameService) { }

  ngOnInit() {
    this.gameService.currentGame$.subscribe(next => {
      this.currentGame = next;
    });
  }

}
