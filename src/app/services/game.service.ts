import { Injectable } from '@angular/core';
import { Game, BowlingFrame } from '../models/game';
import { BehaviorSubject, Subject } from 'rxjs';
import { RollGeneratorService } from './roll-generator.service';

@Injectable({
  providedIn: 'root'
})
export class GameService {
  public currentGame$ = new BehaviorSubject<Game>(this._generateNewGame());
  public previousGames$ = new Subject<Game[]>();
  public activeGame$ = new BehaviorSubject<boolean>(false);

  private currentGame: Game;
  private previousGames: Game[] = [];



  private readonly TOTAL_NUMBER_OF_PINS = 10;
  private currentRolls: number[] = [];

  public constructor(private readonly rollGeneratorService: RollGeneratorService) { }

  public startNewGame(): boolean {
    if (this.currentGame) {
      this.currentGame.timeEnded = new Date();
      this.previousGames.push(this.currentGame);
      this.previousGames$.next(this.previousGames);
    }
    this.currentGame = this._generateNewGame();

    this.currentGame$.next(this.currentGame);
    this.activeGame$.next(true);

    return true;
  }

  public addRoll(): void {
    this._processGame();

    this.currentGame$.next(this.currentGame);
  }

  private _getRemainingPins(): number {
    let knockedDownPins = 0;

    this.currentRolls.forEach(roll => {
      knockedDownPins += roll;
    });

    return this.TOTAL_NUMBER_OF_PINS - knockedDownPins;
  }

  private _addNewRoll() {
    this.currentRolls.push(this.rollGeneratorService.getRandomRoll(this._getRemainingPins()));
  }

  private _addCompletedFrame() {
    const newFrame: BowlingFrame = {
      firstSlot: this.currentRolls[0],
      secondSlot: this.currentRolls[1]
    };

    this.currentGame.frames.push(newFrame);

    this.currentRolls = [];
  }

  private _generateNewGame(): Game {
    return {
      timeStarted: new Date(),
      frames: []
    };
  }

  private _hasAnotherRoll(): boolean {
    const sum = this.currentRolls.reduce((a, b) => a + b, 0);
    return sum === 10;
  }

  private _addCompletedLastFrame(): void {
    const newFrame: BowlingFrame = {
      firstSlot: this.currentRolls[0],
      secondSlot: this.currentRolls[1],
      thirdSlot: this.currentRolls[2]
    };

    this.currentGame.frames.push(newFrame);

    this.currentRolls = [];

    this.currentGame$.next(this.currentGame);
  }

  private _processGame(): void {
    if (this.currentGame.frames.length < 9) {
      if (this.currentRolls.length === 0) {
        this._addNewRoll();
      } else if (this.currentRolls.length === 1) {
        this._addNewRoll();
        this._addCompletedFrame();
      } else {
        this._addCompletedFrame();
        this._addNewRoll();
      }
    } else if (this.currentGame.frames.length === 9) {
      if (this.currentRolls.length === 0) {
        this._addNewRoll();
      } else if (this.currentRolls.length === 1) {
        this._addNewRoll();
        if (!this._hasAnotherRoll()) {
          this._addCompletedLastFrame();
          this.activeGame$.next(false);
        }
      } else if (this._hasAnotherRoll()) {
        this._addNewRoll();
        this._addCompletedLastFrame();
        this.activeGame$.next(false);
      }
    }
  }
}
