
import { TestBed, async } from '@angular/core/testing';
import { RollGeneratorService } from './roll-generator.service';

describe('Service: RollGenerator', () => {
  let service: RollGeneratorService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [RollGeneratorService]
    });
  }));

  beforeEach(() => {
    service = TestBed.get(RollGeneratorService);
  });

  describe('getRandomRoll', () => {
    it('will return a number between 1 and 10', () => {
      const remainingPins = 10;

      const result = service.getRandomRoll(remainingPins);

      expect(result).toBeLessThanOrEqual(remainingPins);
      expect(result).toBeGreaterThanOrEqual(0);
    });

    it('will return a number between 1 and 5', () => {
      const remainingPins = 5;

      const result = service.getRandomRoll(remainingPins);

      expect(result).toBeLessThanOrEqual(remainingPins);
      expect(result).toBeGreaterThanOrEqual(0);
    });

    it('will return a number between 1 and 2', () => {
      const remainingPins = 2;

      const result = service.getRandomRoll(remainingPins);

      expect(result).toBeLessThanOrEqual(remainingPins);
      expect(result).toBeGreaterThanOrEqual(0);
    });
  });
});
