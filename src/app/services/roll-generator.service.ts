import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RollGeneratorService {
  public getRandomRoll(numberOfPinsRemaining: number): number {
    return Math.floor(Math.random() * (numberOfPinsRemaining + 1));
  }
}
