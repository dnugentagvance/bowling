import { Component, OnInit } from '@angular/core';
import { Game } from '../models/game';
import { GameService } from '../services/game.service';

@Component({
  selector: 'app-recent-games',
  templateUrl: './recent-games.component.html',
  styleUrls: ['./recent-games.component.css']
})
export class RecentGamesComponent implements OnInit {
  public recentGames: Game[] = [];

  public constructor(private gameService: GameService) { }

  public ngOnInit(): void {
    this.gameService.previousGames$.subscribe(next => {
      this.recentGames = next;
    });
  }

}
