import { Component } from '@angular/core';
import { GameService } from './services/game.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public activeGame$: Observable<boolean>;

  public constructor(private readonly gameService: GameService) {
    this.activeGame$ = this.gameService.activeGame$;
  }

  public startNewGame(): void {
    this.gameService.startNewGame();
  }

  public addRoll(): void {
    this.gameService.addRoll();
  }

}
