import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ScoreboardComponent } from './scoreboard/scoreboard.component';
import { RecentGamesComponent } from './recent-games/recent-games.component';

@NgModule({
   declarations: [
      AppComponent,
      ScoreboardComponent,
      RecentGamesComponent
   ],
   imports: [
      BrowserModule
   ],
   providers: [],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
