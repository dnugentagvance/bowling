export interface StandardFrame {
    firstSlot: number;
    secondSlot: number;
}
