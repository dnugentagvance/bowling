import { StandardFrame } from './standard-frame';
import { LastFrame } from './last-frame';

export type BowlingFrame = StandardFrame | LastFrame;

export interface Game {
    timeStarted: Date;
    timeEnded?: Date;
    frames: BowlingFrame[];
}
