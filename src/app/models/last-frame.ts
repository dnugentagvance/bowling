import { StandardFrame } from './standard-frame';

export interface LastFrame extends StandardFrame {
    thirdSlot: number;
}
